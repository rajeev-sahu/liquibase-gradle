package com.db.version.dbversion.entities;

import lombok.Data;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity
@Data
public class Users {

    @Id
    private int id;

    private String name;

    private String city;

    private int age;




}
