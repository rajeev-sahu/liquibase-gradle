package com.db.version.dbversion.entities;

public enum Role {
    ADMIN,
    WORKER;
}
