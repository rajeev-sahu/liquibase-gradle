package com.db.version.dbversion;

import com.db.version.dbversion.entities.Role;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Arrays;

@SpringBootApplication
public class DbVersionApplication {

    public static void main(String[] args) {

        SpringApplication.run(DbVersionApplication.class, args);


    }

}

