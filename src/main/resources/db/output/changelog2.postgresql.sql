-- *********************************************************************
-- Update Database Script
-- *********************************************************************
-- Change Log: src/main/resources/db/changelog-master.xml
-- Ran at: 2/13/19 7:12 PM
-- Against: rajeev.s@jdbc:postgresql://localhost:5432/flywaydb
-- Liquibase version: 3.3.5
-- *********************************************************************

-- Create Database Lock Table
CREATE TABLE public.databasechangeloglock (ID INT NOT NULL, LOCKED BOOLEAN NOT NULL, LOCKGRANTED TIMESTAMP WITHOUT TIME ZONE, LOCKEDBY VARCHAR(255), CONSTRAINT PK_DATABASECHANGELOGLOCK PRIMARY KEY (ID));

-- Initialize Database Lock Table
DELETE FROM public.databasechangeloglock;

INSERT INTO public.databasechangeloglock (ID, LOCKED) VALUES (1, FALSE);

-- Lock Database
UPDATE public.databasechangeloglock SET LOCKED = TRUE, LOCKEDBY = 'fe80:0:0:0:e2bf:fd09:e907:bf%utun0 (fe80:0:0:0:e2bf:fd09:e907:bf%utun0)', LOCKGRANTED = '2019-02-13 19:12:36.937' WHERE ID = 1 AND LOCKED = FALSE;

-- Create Database Change Log Table
CREATE TABLE public.databasechangelog (ID VARCHAR(255) NOT NULL, AUTHOR VARCHAR(255) NOT NULL, FILENAME VARCHAR(255) NOT NULL, DATEEXECUTED TIMESTAMP WITHOUT TIME ZONE NOT NULL, ORDEREXECUTED INT NOT NULL, EXECTYPE VARCHAR(10) NOT NULL, MD5SUM VARCHAR(35), DESCRIPTION VARCHAR(255), COMMENTS VARCHAR(255), TAG VARCHAR(255), LIQUIBASE VARCHAR(20));

-- Changeset src/main/resources/db/changelog/create-users-table.xml::create-users-table::rajeev.s
-- Create users Table
CREATE TABLE users (
            id SERIAL NOT NULL,
            name TEXT NOT NULL,
            city TEXT NOT NULL,
            age INTEGER NOT NULL);

INSERT INTO public.databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, LIQUIBASE) VALUES ('create-users-table', 'rajeev.s', 'src/main/resources/db/changelog/create-users-table.xml', NOW(), 1, '7:3d559c86141d403b064491b71c148411', 'sql', 'Create users Table', 'EXECUTED', '3.3.5');

-- Changeset src/main/resources/db/changelog/insert-users-table.xml::insert-users::rajeev.s
-- Insert users
INSERT INTO users (name,city,age) VALUES ('Kirit.p','Mumbai',37),
            ('Manish.m','Mumbai',40),
            ('Supriya.m','Mumbai',37),
            ('Rahul.go','Mumbai',30);

INSERT INTO public.databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, LIQUIBASE) VALUES ('insert-users', 'rajeev.s', 'src/main/resources/db/changelog/insert-users-table.xml', NOW(), 2, '7:6ddf5806ff3bce447013c3f1f2dc38ad', 'sql', 'Insert users', 'EXECUTED', '3.3.5');

-- Changeset src/main/resources/db/changelog/add-column-gender.xml::add-column-gender::rajeev.s
-- Add Column gender
ALTER TABLE users ADD gender TEXT NOT NULL DEFAULT 'MALE';

INSERT INTO public.databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, LIQUIBASE) VALUES ('add-column-gender', 'rajeev.s', 'src/main/resources/db/changelog/add-column-gender.xml', NOW(), 3, '7:27c487250646e42c2909dfe330a5026f', 'sql', 'Add Column gender', 'EXECUTED', '3.3.5');

-- Changeset src/main/resources/db/changelog/insert-more-users.sql::raw::includeAll
INSERT INTO users (name,city,age) VALUES ('gopal.r','Mumbai',40);

INSERT INTO public.databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, LIQUIBASE) VALUES ('raw', 'includeAll', 'src/main/resources/db/changelog/insert-more-users.sql', NOW(), 4, '7:86d1f28948b74d908e8e461cee21fa30', 'sql', '', 'EXECUTED', '3.3.5');

-- Changeset src/main/resources/db/changelog/insert-more-users2.sql::raw::includeAll
INSERT INTO users (name,city,age) VALUES ('birju.s','Mumbai',35);

INSERT INTO public.databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, LIQUIBASE) VALUES ('raw', 'includeAll', 'src/main/resources/db/changelog/insert-more-users2.sql', NOW(), 5, '7:78cb640b4253f2a251b5c85c9a2bca28', 'sql', '', 'EXECUTED', '3.3.5');

-- Release Database Lock
UPDATE public.databasechangeloglock SET LOCKED = FALSE, LOCKEDBY = NULL, LOCKGRANTED = NULL WHERE ID = 1;

