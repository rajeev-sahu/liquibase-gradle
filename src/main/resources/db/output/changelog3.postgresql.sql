-- *********************************************************************
-- Update Database Script
-- *********************************************************************
-- Change Log: src/main/resources/db/changelog-master.xml
-- Ran at: 2/13/19 7:17 PM
-- Against: rajeev.s@jdbc:postgresql://localhost:5432/flywaydb
-- Liquibase version: 3.3.5
-- *********************************************************************

-- Lock Database
UPDATE public.databasechangeloglock SET LOCKED = TRUE, LOCKEDBY = 'fe80:0:0:0:e2bf:fd09:e907:bf%utun0 (fe80:0:0:0:e2bf:fd09:e907:bf%utun0)', LOCKGRANTED = '2019-02-13 19:17:46.046' WHERE ID = 1 AND LOCKED = FALSE;

-- Changeset src/main/resources/db/changelog/insert-more-users3.sql::raw::includeAll
INSERT INTO users (name,city,age) VALUES ('kartik.m','Mumbai',35);

INSERT INTO public.databasechangelog (ID, AUTHOR, FILENAME, DATEEXECUTED, ORDEREXECUTED, MD5SUM, DESCRIPTION, COMMENTS, EXECTYPE, LIQUIBASE) VALUES ('raw', 'includeAll', 'src/main/resources/db/changelog/insert-more-users3.sql', NOW(), 6, '7:1b98f4bc2d8227b295c38cd364c265bc', 'sql', '', 'EXECUTED', '3.3.5');

-- Release Database Lock
UPDATE public.databasechangeloglock SET LOCKED = FALSE, LOCKEDBY = NULL, LOCKGRANTED = NULL WHERE ID = 1;

